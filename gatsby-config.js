/**
 * Configure your Gatsby site with this file.
 *
 * See: https://www.gatsbyjs.org/docs/gatsby-config/
 */

module.exports = {
    plugins: [
        {
            resolve: `gatsby-source-filesystem`,
            options: {
                name: `src`,
                path: `${__dirname}/src/`,
            },
        },
        `gatsby-transformer-remark`,
        `gatsby-plugin-emotion`,
        {
            resolve: `gatsby-plugin-typography`,
            options: {
                pathToConfigModule: `src/utils/typography`,
            },
        },
    ],
    siteMetadata: {
        title: `ppavia.gitlab.io`,
        author: `P. PAVIA`,
        description: `Nothing concrete for now...`,
        social: [
            {
                name: `twitter`,
                url: `https://twitter.com/gatsbyjs`,
            },
            {
                name: `github`,
                url: `https://github.com/gatsbyjs`,
            },
        ],
    },
}
