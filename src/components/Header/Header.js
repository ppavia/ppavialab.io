import React from 'react';
import Toolbar from '../Toolbar/Toolbar';
import classes from './Header.module.css';

const Header = (props) => {
    return (
        <header className={classes.Header}>
            <Toolbar
                title={props.title}
            />
            <section className={classes.HeaderBlockTitle} >
                <div className={classes.HeaderParentTitle}>
                    <div className={classes.HeaderTitle}>
                        {props.title}
                    </div>
                </div>
            </section>
        </header>
    );
};

export default Header;
