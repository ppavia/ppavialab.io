import React from 'react';
import { StaticQuery, graphql } from 'gatsby';
import Header from '../Header/Header';
import classes from './Layout.module.css';

const Layout = (props) => (
    <StaticQuery
        query={graphql`
            query HeadingQuery {
                site {
                    siteMetadata {
                        title
                    }
                }
            }
        `}
        render={data => (
            <div>
                <Header
                    title={data.site.siteMetadata.title}
                />
                <main className={classes.Main}>
                    {props.children}
                </main>
            </div>
        )}
    />

)

export default Layout;


