import React from 'react';
import imgLogo from '../../assets/images/burger-logo.png';
import classes from './Logo.module.css';

const logo = (props) => (
    <div className={classes.Logo}>
        <img src={imgLogo} alt={props.altTitle} />
    </div>
);

export default logo;