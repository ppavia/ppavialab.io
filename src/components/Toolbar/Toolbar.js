import React from 'react';
import { Link } from 'gatsby';
import classes from './Toolbar.module.css';
// import Logo from '../Logo/Logo';
import NavigationItems from '../Navigation/NavigationItems/NavigationItems';

const Toolbar = (props) => (
    <div className={classes.Toolbar}>
        <Link to="/">
            <h3>{props.title}</h3>
        </Link>
        {/* <Logo
            altTitle={props.title}
        /> */}
        <nav>
            <NavigationItems />
        </nav>
    </div>
);

export default Toolbar;