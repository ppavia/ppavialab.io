import React from 'react';
import { Link } from 'gatsby';
import { rhythm } from '../../utils/typography';
import Aux from '../../containers/hoc/Aux';
import classes from './Home.module.css';

const home = (props) => {
    return (
        <Aux>
            <div className={classes.Title}>
                <p>Welcome to the ppavia blog site!</p>
                <p>Nothing to say now but some articles come soon.</p>
            </div>
            <div className={classes.Blogs}>
                <h4>{props.allMarkdownRemark.totalCount} Posts</h4>
                {props.allMarkdownRemark.edges.map(({ node }) => {
                    return (
                        <Link to={node.fields.slug} key={node.id}>
                            <h3>
                                {node.frontmatter.title}{" "}
                                <span>— {node.frontmatter.date}</span>
                            </h3>
                            <p>{node.excerpt}</p>
                        </Link>
                    )
                })}
            </div>
        </Aux >
    );
}
export default home;