import React, { useEffect } from 'react';
import Layout from '../Layout/Layout';
import * as d3 from 'd3';
import classes from './D3Layout.module.css';

export default function D3Layout(props) {
    useEffect(() => {
        getD3();
    });
    return (
        <Layout>
            {props.children}
            <svg viewBox='true' className={classes.PieMarketCap}>
            </svg>
        </Layout>
    );
}

const getD3 = () => {

    let nameCoin = [];

    const margin = { top: 10, right: 10, bottom: 10, left: 10 },
        width = 445 - margin.left - margin.right,
        height = 445 - margin.top - margin.bottom;

    let data = [2, 4, 8, 50];

    const svg = d3.select("svg")
        .attr("viewBox", `0 0 ${width + margin.left + margin.right} ${height + margin.top + margin.bottom}`);

    const radius = Math.min(width, height) / 2;
    const g = svg.append("g").attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");
    console.log(`width = ${width}`);
    console.log(`height = ${height}`);

    const color = d3.scaleOrdinal(['#4daf4a', '#377eb8', '#ff7f00', '#984ea3', '#e41a1c']);

    // Generate the pie
    let pie = d3.pie()
        .value((d) => d.percent);

    // Generate the arcs
    let arc = d3.arc()
        .outerRadius(radius)
        .innerRadius(0);

    //Generate groups
    let arcs = g.selectAll("arc")
        .data(pie(data))
        .enter()
        .append("g")
        .attr("class", "arc")

    //Draw arc paths
    arcs.append("path")
        .attr("fill", function (d, i) {
            return color(i);
        })
        .attr("d", arc);

    d3.json("https://ppa-coinmarketcap.herokuapp.com/coinmarketcap/root").then(function (data) {

        let i = 0;
        if (data.data && data.data instanceof Object) {
            for (let coin in data.data) {
                //console.log(data.data[coin].quote.USD);
                nameCoin[i] = { name: data.data[coin].name, value: data.data[coin].quote.USD };
                i++;
            }
        }
        console.log(nameCoin);
    })
        .then(() => {
        });
}