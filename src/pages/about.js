import React from 'react';
import Layout from '../components/Layout/Layout';
import { graphql } from 'gatsby';

export default ({ data }) => (
    <Layout>
        <h1>About {data.site.siteMetadata.title}</h1>
        <p>
            I’m good enough, I’m smart enough, and gosh darn it, people like me!
        </p>
    </Layout>
)
export const query = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
  }
`
