import React from 'react';
import { graphql } from 'gatsby';
import Layout from '../components/Layout/Layout';
import Home from '../components/Home/Home';

export default ({ data }) => {
    //console.log(data)
    return (
        <Layout>
            <Home
                allMarkdownRemark={data.allMarkdownRemark}
            />
        </Layout>
    )
}

export const query = graphql`
  query {
    allMarkdownRemark (
        filter: {
            fields: {
                slug: { regex: "/blog-post/" }
            }
        }
    ) {
      totalCount
      edges {
        node {
          id
          frontmatter {
            title
            date(formatString: "DD MMMM, YYYY")
          }
          excerpt
          fields {
            slug
            toto
          }
        }
      }
    }
  }
`
