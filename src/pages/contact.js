import React from 'react';
import Layout from '../components/Layout/Layout';

export default function Contact() {
    return (
        <Layout>
            <h1>I'd love to talk! Email me at the address below</h1>
            <p>
                <a href='mailto:me@example.com'>ppavia@protonmail.com</a>
            </p>
        </Layout>
    )
}
