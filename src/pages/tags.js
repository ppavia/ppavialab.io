import React from 'react';
import Aux from '../containers/hoc/Aux';

export default function Tags () {
    return (
        <Aux>
            <div>
                <h1>Tags !</h1>
            </div>
        </Aux>
    );
};