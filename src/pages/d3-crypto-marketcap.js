import React from "react"
import D3Layout from '../components/D3Layout/D3Layout';
import { graphql } from 'gatsby';

export default ({ data }) => {
    const post = data.markdownRemark;
    return (
        <D3Layout>
            <div>
                <h1>{post.frontmatter.title}</h1>
                <p>{post.frontmatter.date}</p>
            </div>
        </D3Layout>
    )
}
export const query = graphql`
  query {
    markdownRemark(fields: {
        slug: { regex: "/blog-post/" }
    }) {
      html
      frontmatter {
        title
        date(formatString: "DD MMMM, YYYY")
      }
      excerpt
    }
  }
`
