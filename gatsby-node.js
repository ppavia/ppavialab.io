const path = require(`path`)
const { createFilePath } = require(`gatsby-source-filesystem`)
exports.onCreateNode = ({ node, getNode, actions }) => {
    const { createNodeField } = actions
    if (node.internal.type === `MarkdownRemark`) {
        const slug = createFilePath({ node, getNode, basePath: `blog-post` })
        createNodeField({
            node,
            name: `slug`,
            value: slug,
        })
        createNodeField({
            node,
            name: `toto`,
            value: `toto as toto`,
        })
    }
}

exports.createPages = async ({ graphql, actions }) => {
    const { createPage } = actions
    const resultPost = await graphql(`
        query {
            allMarkdownRemark (
                filter: {
                    fields: {
                        slug: { regex: "/blog-post/" }
                    }
                }
            ){
                edges {
                    node {
                        fields {
                            slug
                        }
                    }
                }
            }
        }
`)
    const slug_d3_regex = RegExp('d3-crypto-marketcap');
    resultPost.data.allMarkdownRemark.edges.forEach(({ node }) => {
        console.log(node);
        // d3 specific template
        if (!slug_d3_regex.test(node.fields.slug)) {
            createPage({
                path: node.fields.slug,
                component: path.resolve(`./src/templates/blog-post.js`),
                context: {
                    // Data passed to context is available
                    // in page queries as GraphQL variables.
                    slug: node.fields.slug,
                },
            })
        }
        else {
            createPage({
                path: node.fields.slug,
                component: path.resolve(`./src/pages/d3-crypto-marketcap.js`),
                context: {
                    // Data passed to context is available
                    // in page queries as GraphQL variables.
                    slug: node.fields.slug,
                },
            })
        }
    })
}
